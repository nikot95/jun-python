"""
Подключить модуль requests
Подключить модуль bs4
Отправить запрос на страницу hltv.org
Получить список всех сегодняшних матчей
Вывести на экран в формате `Team1 vs Team2 hour:minute`
"""
import bs4
import requests
from bs4 import BeautifulSoup


if __name__ == '__main__':
    response = requests.get("https://hltv.org/")
    soup = BeautifulSoup(response.text)
    teamrows = soup.find("div", class_="top-border-hide")
    time_match = soup.find("div", class_="middleExtra")
    print({teamrows + time_match})












"""
Написать функцию для вычисления суммы двух элементов (элементы передавать параметрами) 
"""
def two_values(one_values, next_values):
    print(f'Sum: {one_values + next_values}')
two_values(33, 66)

"""
Написать функцию для вычисления суммы двух элементов (элементы передавать параметрами), 
если неизвестно коли-во аргументов
"""

def sum_elementov(*args): #Если неизвестно коли-во аргументов
    sum_element = 0 #новый список в который будет складывать все элементы
    for element_sum in args:
        sum_element += element_sum
    print(f"Sum element: {sum_element}")

sum_elementov(12, 12, 100)