# ~/home/nikk/PycharmProjects/pythonProjectNik/less2.py'
# coding: utf-8

# Задание 1. В цикле вывести список всех элементов на экран

list_example = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

for i in range(len(list_example)):
    print(list_example[i])

# Задание 2. Вывести на экран список не кратных 5 элементов (всего 15 элементов от 1 до 15)

new_list = [x for x in range(len(list_example)) if x % 5 == 0]
print(new_list)
